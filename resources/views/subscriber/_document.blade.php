
<div class="panel panel-default">
  <div class="panel-heading">{{ $document->title}}</div>
  <div class="panel-body">
    <dl>
      <dt>Published by:</dt>
      <dd>{{ $document->publisher['publisher_name'] }}</dd>
    </dl>

    @if( $isSubscribed )
      <form class="form-inline" method="POST" action="{{ route('unsubscribe', $document->id) }}">
        {{csrf_field()}}
        <button type="submit" class="btn btn-warning">Unsubscribe</button>
      </form>
    @else
    <form class="form-inline" method="POST" action="{{ route('subscribe', $document->id) }}">
      {{csrf_field()}}
      <button type="submit" class="btn btn-primary">Subscribe</button>
    </form>
    @endif
  </div>
</div>
