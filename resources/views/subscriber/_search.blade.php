
  <form class="form-inline" method="GET" action="{{route('search')}}">
    <div class="form-group">
      <label class="sr-only" for="search">Search for a document</label>
      <div class="input-group">
        <div class="input-group-addon"><span class="glyphicon glyphicon-search"></span></div>
        <input type="text" class="form-control" id="search" name="q" placeholder="Name of the document">
      </div>
    </div>
    <button type="submit" class="btn btn-primary">Search</button>
  </form>
