
@if (!Auth::guest())
  <nav class="navbar navbar-default">
    <div class="container-fluid">

      <div class="navbar-header">
        <a class="navbar-brand" href="#">Subscribe</a>
      </div>

      <ul class="nav navbar-nav navbar-right">
        <li>
          <a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a>
        </li>
      </ul>

    </div>
  </nav>
@endif
