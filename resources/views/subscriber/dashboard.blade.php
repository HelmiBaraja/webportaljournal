@extends('base')

@section('content')

  @include('subscriber/_search')

  <div class="row">
    <h2>Available Documents</h2>
    @foreach($documents as $document)
      <div class="col-md-2 document">
        @include('subscriber/_document', ['isSubscribed' => false])
      </div>
    @endforeach
  </div>

  <hr/>

  <div class="row">
    @if( $subscriptions->count() < 1)
      <div class="col-md-12">You have no subscription</div>
    @else
      <h2>Your Subscriptions</h2>
      @foreach($subscriptions as $subscription)
        <div class="col-md-2 subscription">
        @include('subscriber/_document', ['isSubscribed' => true, 'document' => $subscription])
        </div>
      @endforeach
    @endif

  </div>

@endsection
