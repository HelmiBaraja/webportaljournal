@extends('base')

@section('content')

  <div class="row">
    @forelse($documents as $document)
      <div class="col-md-2">
        @include('subscriber/_document', ['isSubscribed' => false])
      </div>
    @empty
      <div class="col-md-12">
        <p class="bg-warning">
          No search result found. Please refine your search result.
        </p>
        @include('subscriber/_search')
      </div>
    @endforelse
  </div>

@endsection
