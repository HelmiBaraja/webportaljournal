<table class="table table-responsive" id="documents-table">
    <thead>
        <th>Title</th>
        <th>Status</th>
        <th>Publisher</th>
        <th>Uploaded</th>
        <th>Filename</th>
        @if(Auth::user()->is_publisher )
          <th colspan="3">Action</th>
        @endif

    </thead>
    <tbody>
    @foreach($documents as $document)
        <tr>
            <td>{!! $document->title !!}</td>
            <td>{!! $document->status !!}</td>
            <td>{!! $document->publisher->publisher_name !!}</td>
            <td>{!! $document->created_at!!}</td>
            <td>{!! $document->filename !!}</td>
              @if(Auth::user()->is_publisher )
                <td>
                    {!! Form::open(['route' => ['documents.destroy', $document->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{!! route('documents.show', [$document->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{!! route('documents.edit', [$document->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
              @endif

        </tr>
    @endforeach
    </tbody>
</table>
