<!-- Publisher Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title :') !!}
    {!! Form::text('title', null, ['class' => 'form-control']) !!}
</div>


<div class="form-group col-sm-6">
    {!! Form::label('status', 'Status :') !!}
    {!! Form::select('status', [true=>'active', false=>'inactive']); !!}
</div>


<div class="form-group col-sm-6">
    {!! Form::label('document', 'File :') !!}
    {!! Form::file('document') !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('documents.index') !!}" class="btn btn-default">Cancel</a>
</div>
