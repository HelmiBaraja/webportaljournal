@extends('layouts.app')

@section('content')
        <h1 class="pull-left">documents</h1>
        @if(Auth::user()->is_publisher )

          <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('documents.create') !!}">Add New</a>

        @endif


        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        @include('documents.table')

@endsection
