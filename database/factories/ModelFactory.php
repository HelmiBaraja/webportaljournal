<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
 */

/**
 */
$factory->defineAs(App\User::class, 'publisher', function (Faker\Generator $faker) {
    return [
        'email' => $faker->safeEmail,
        'password' => bcrypt('secret'),
        'is_publisher' => true,
        'is_public_user' =>false,
        'remember_token' => str_random(10),
    ];
});


/**
 */
$factory->defineAs(App\User::class, 'publicUser', function (Faker\Generator $faker) {
    return [
        'email' => $faker->safeEmail,
        'password' => bcrypt('secret'),
        'is_publisher' => false,
        'is_public_user' =>true,
        'remember_token' => str_random(10),
    ];
});


/**
 * seed document
 */
$factory->defineAs(App\Models\document::class, 'document', function (Faker\Generator $faker) {

    $randomFile = $faker->file($sourceDir=resource_path().'/fakerFile/pdf', $targetDir=storage_path('pdfLibrary'), false);
    $size = filesize(storage_path("pdfLibrary/".$randomFile));

    return [
        'title' => $faker->name,
        'status' => true,
        'filename' => $randomFile,
        'publisher_id' => $faker->numberBetween($min = 1, $max = 2),
    ];
});
