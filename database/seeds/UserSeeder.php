<?php

use Illuminate\Database\Seeder;

use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


      //create admin user
      $user = new User;
      $user->email = 'gujarat.santana@gmail.com';
      $user->password = bcrypt('secret');
      $user->is_publisher = true;
      $user->is_public_user = false;
      $user->publisher_name = 'Gujarat Santana';
      $user->save();

      //create 2 publisher
      $feedback = factory(App\User::class, 'publisher', 1)->create();

      //create 1 public user
      $feedback = factory(App\User::class, 'publicUser', 1)->create();
    }
}
