<?php

use App\Models\document;
use App\Repositories\documentRepository;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class documentRepositoryTest extends TestCase
{
    use MakedocumentTrait, ApiTestTrait, DatabaseTransactions;

    /**
     * @var documentRepository
     */
    protected $documentRepo;

    public function setUp()
    {
        parent::setUp();
        $this->documentRepo = App::make(documentRepository::class);
    }

    /**
     * @test create
     */
    public function testCreatedocument()
    {
        $document = $this->fakedocumentData();
        $createddocument = $this->documentRepo->create($document);
        $createddocument = $createddocument->toArray();
        $this->assertArrayHasKey('id', $createddocument);
        $this->assertNotNull($createddocument['id'], 'Created document must have id specified');
        $this->assertNotNull(document::find($createddocument['id']), 'document with given id must be in DB');
        $this->assertModelData($document, $createddocument);
    }

    /**
     * @test read
     */
    public function testReaddocument()
    {
        $document = $this->makedocument();
        $dbdocument = $this->documentRepo->find($document->id);
        $dbdocument = $dbdocument->toArray();
        $this->assertModelData($document->toArray(), $dbdocument);
    }

    /**
     * @test update
     */
    public function testUpdatedocument()
    {
        $document = $this->makedocument();
        $fakedocument = $this->fakedocumentData();
        $updateddocument = $this->documentRepo->update($fakedocument, $document->id);
        $this->assertModelData($fakedocument, $updateddocument->toArray());
        $dbdocument = $this->documentRepo->find($document->id);
        $this->assertModelData($fakedocument, $dbdocument->toArray());
    }

    /**
     * @test delete
     */
    public function testDeletedocument()
    {
        $document = $this->makedocument();
        $resp = $this->documentRepo->delete($document->id);
        $this->assertTrue($resp);
        $this->assertNull(document::find($document->id), 'document should not exist in DB');
    }
}
