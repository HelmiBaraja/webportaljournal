<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class documentApiTest extends TestCase
{
    use MakedocumentTrait, ApiTestTrait, WithoutMiddleware, DatabaseTransactions;

    /**
     * @test
     */
    public function testCreatedocument()
    {
        $document = $this->fakedocumentData();
        $this->json('POST', '/api/v1/documents', $document);

        $this->assertApiResponse($document);
    }

    /**
     * @test
     */
    public function testReaddocument()
    {
        $document = $this->makedocument();
        $this->json('GET', '/api/v1/documents/'.$document->id);

        $this->assertApiResponse($document->toArray());
    }

    /**
     * @test
     */
    public function testUpdatedocument()
    {
        $document = $this->makedocument();
        $editeddocument = $this->fakedocumentData();

        $this->json('PUT', '/api/v1/documents/'.$document->id, $editeddocument);

        $this->assertApiResponse($editeddocument);
    }

    /**
     * @test
     */
    public function testDeletedocument()
    {
        $document = $this->makedocument();
        $this->json('DELETE', '/api/v1/documents/'.$document->id);

        $this->assertApiSuccess();
        $this->json('GET', '/api/v1/documents/'.$document->id);

        $this->assertResponseStatus(404);
    }
}
