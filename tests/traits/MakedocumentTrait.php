<?php

use Faker\Factory as Faker;
use App\Models\document;
use App\Repositories\documentRepository;

trait MakedocumentTrait
{
    /**
     * Create fake instance of document and save it in database
     *
     * @param array $documentFields
     * @return document
     */
    public function makedocument($documentFields = [])
    {
        /** @var documentRepository $documentRepo */
        $documentRepo = App::make(documentRepository::class);
        $theme = $this->fakedocumentData($documentFields);
        return $documentRepo->create($theme);
    }

    /**
     * Get fake instance of document
     *
     * @param array $documentFields
     * @return document
     */
    public function fakedocument($documentFields = [])
    {
        return new document($this->fakedocumentData($documentFields));
    }

    /**
     * Get fake data of document
     *
     * @param array $postFields
     * @return array
     */
    public function fakedocumentData($documentFields = [])
    {
        $fake = Faker::create();

        return array_merge([
            'title' => $fake->word,
            'status' => $fake->word,
            'publisher_id' => $fake->word,
            'created_at' => $fake->word,
            'updated_at' => $fake->word
        ], $documentFields);
    }
}
