<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;


class SubscriptionTest extends TestCase
{
    use DatabaseTransactions;

    public function testLoginAsSubscriber()
    {
        $user = factory(App\User::class, 'publicUser')->create();
        $this->visit('/login')
            ->type($user->email, 'email')
            ->type('secret', 'password')
            ->press('Login')
            ->seePageIs('/dashboard');
    }

    public function testSubscriptionSearch()
    {
        $user = factory(App\User::class, 'publicUser')->create();
        $document = factory(App\Models\document::class, 'document')->create();

        $this->actingAs($user)
            ->visit('/search?q=' . $document->title)
            ->see($document->title);
    }

    public function testSubscriptionDashBoard()
    {
        $user = factory(App\User::class, 'publicUser')->create();
        $document = factory(App\Models\document::class, 'document')->create();

        $user->subscriptions()->attach($document);

        $this->actingAs($user)
            ->visit('/dashboard')
            ->countElements('.subscription', 1)
            ->see($document->title);
    }

    public function testSubscribeFromSearch()
    {
        $user = factory(App\User::class, 'publicUser')->create();
        $document = factory(App\Models\document::class, 'document')->create();

        $this->actingAs($user)
            ->visit('/search?q=' . $document->title)
            ->see($document->title)
            ->press('Subscribe');

        $this->actingAs($user)
            ->visit('/dashboard')
            ->countElements('.subscription', 1)
            ->see($document->title);
    }

    public function testunSubscribeFromDashboard()
    {
        $user = factory(App\User::class, 'publicUser')->create();
        $document = factory(App\Models\document::class, 'document')->create();
        $user->subscriptions()->attach($document);

        $this->actingAs($user)
            ->visit('/dashboard')
            ->press('Unsubscribe');

        $this->actingAs($user)
            ->visit('/dashboard')
            ->countElements('.subscription', 0);
    }

    public function testSeeDocumentAsAnon()
    {
        $document = factory(App\Models\document::class, 'document')->create();
        $this->get('/api/document/'.$document->id, ['Accept' => 'application/json'])
            ->assertResponseStatus(401);
    }

    public function testSeeDocumentAsSub()
    {
        $document = factory(App\Models\document::class, 'document')->create();
        $user = factory(App\User::class, 'publicUser')->create();

        $this->actingAs($user)
            ->get('/api/document/'.$document->id)
            ->assertResponseStatus(200)
            ->seeHeader('content-type', 'application/pdf');
    }
}
