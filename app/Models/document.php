<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="document",
 *      required={"title", "status", "publisher_id"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="title",
 *          description="title",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="status",
 *          description="status",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="publisher_id",
 *          description="publisher_id",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class document extends Model
{
    use SoftDeletes;

    public $table = 'documents';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'title',
        'status',
        'filename',
        'publisher_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'string',
        'status' => 'boolean',
        'publisher_id' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required',
        'status' => 'required',
        'document' => 'required'
    ];

    //relationship
    public function publisher()
    {
      return $this->belongsTo('App\User', 'publisher_id', 'id');
    }

    public function subscribers(){
        return $this->belongsToMany('App\User', 'subscriptions', 'document_id', 'user_id');
    }
}
