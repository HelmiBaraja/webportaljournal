<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;

class TokenManager{

    public function generateToken($userId)
    {
        $timestamp = Carbon::now()->toAtomString();
        $string = $this->generateString($userId, $timestamp);
        $hash = $this->hash($string);

        return $string .'|'.$hash;
    }

    protected function generateString($userId, $timestamp)
    {
        return $userId . '|' . $timestamp;
    }

    protected function hash($string)
    {
        $secret = config('app.key');
        return Hash::make($string.$secret);
    }

    protected function verifyHash($string, $hash)
    {
        $secret = config('app.key');
        return Hash::check($string.$secret, $hash);
    }

    /**
     * Check auth token.
     * return user id if successful.
     */
    public function verifyToken($token)
    {
        $parts = explode('|', $token);
        $userId = $parts[0];
        $timestamp = $parts[1];
        $hash = $parts[2];

        $string = $this->generateString($userId, $timestamp);

        if($this->verifyHash($string, $hash)){
            return $userId;
        }
        return false;
    }

}
