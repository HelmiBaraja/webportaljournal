<?php

namespace App\Repositories;

use App\Models\document;
use InfyOm\Generator\Common\BaseRepository;

class documentRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return document::class;
    }
}
