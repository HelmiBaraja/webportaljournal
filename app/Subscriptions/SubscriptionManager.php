<?php namespace App\Subscriptions;

use App\Models\document;

class SubscriptionManager{

    public function getSubscriptions($user)
    {
        return $user->subscriptions()->with('publisher')->get();
    }

    public function subscribeToDocument($user, $documentId)
    {
        $document = document::find($documentId);

        if( is_null($document) ){
            throw new SubscriptionException('Document not found');
        }

        $user->subscriptions()->attach($document);
        return $document;
    }

    public function unsubscribeToDocument($user, $documentId)
    {
        $document = $user->subscriptions()->find($documentId);

        if( is_null($document) ){
            throw new SubscriptionException('Document not found');
        }

        $user->subscriptions()->detach($documentId);
        return $document;
    }

    public function getSubscriptionOptions($userId, $search = null)
    {
        $documents = document::with('publisher')
            ->whereDoesntHave('subscribers', function($q) use($userId){
                $q->where('users.id', $userId);
            });

        if( !is_null($search) ){
            $documents = $documents->where('title', 'like', '%'.$search.'%');
        }

        return $documents;
    }

}
