<?php namespace App\Subscriptions;

use App\Transformers\BaseTransformer;
use App\Models\document;

class SubscriptionTransformer extends BaseTransformer{

    public function transform(document $document){
        return [
            'journalId' => $document->id,
            'journalTitle' => $document->title,
            'uploadedOn' => $this->formatDateTime($document->created_at),
            'publisherId' => $document->publisher_id,
            'publisherName' => $document->publisher->publisher_name
        ];
    }

}
