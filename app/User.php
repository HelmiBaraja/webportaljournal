<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    protected $table = 'users';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',  'password', 'publisher_name', 'is_publisher', 'is_public_user'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function documents(){
        return $this->hasMany('App\Models\document','publisher_id');
    }

    public function subscriptions(){
        return $this->belongsToMany('App\Models\document', 'subscriptions', 'user_id', 'document_id');
    }

    public function getRoleAttribute()
    {
        if( $this->is_publisher ){
            return 1;
        }

        return 2;
    }
}
