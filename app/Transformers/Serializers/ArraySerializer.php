<?php namespace App\Transformers\Serializers;

use League\Fractal\Serializer\ArraySerializer as FractalSerializer;

class ArraySerializer extends FractalSerializer{

    /**
     * Serialize a collection. Basically the same thing, but not using the
     * data namespace.
     *
     * @param string $resourceKey
     * @param array  $data
     *
     * @return array
     */
    public function collection($resourceKey, array $data)
    {
        if( $resourceKey ){
            return array($resourceKey => $data);
        }

        return $data;
    }

}
