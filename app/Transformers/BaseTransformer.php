<?php namespace App\Transformers;

use League\Fractal\TransformerAbstract;

abstract class BaseTransformer extends TransformerAbstract{

    protected $dateFormat = 'Y-m-d';

    protected $dateTimeFormat = 'Y-m-d h:m:s';

    protected function formatDate($date)
    {
        if( is_null($date) ){
            return null;
        }
        return $date->toIso8601String();
    }

    protected function formatDateTime($datetime)
    {
        if( is_null($datetime) ){
            return null;
        }

        return $datetime->toIso8601String();
    }

}
