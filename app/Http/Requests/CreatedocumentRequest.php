<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use App\Models\document;
use Illuminate\Support\Facades\Auth;

class CreatedocumentRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->is_publisher;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return document::$rules;
    }
}
