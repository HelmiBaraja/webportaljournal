<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where all API routes are defined.
|
*/





Route::resource('api/documents', 'documentAPIController');

Route::post('/api/login', 'AuthController@login');

Route::group(['middleware' => 'auth.subscriber'], function(){
    Route::get('/api/subscriptions', 'SubscriptionAPIController@getSubscription');
    Route::get('/api/document/{id}', 'SubscriptionAPIController@viewFile');
});
