<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\JsonResponse;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use App\Transformers\Serializers\ArraySerializer;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    protected function renderJson($data, $transformer = null)
    {
        if(! is_null($transformer) ){
            $fractal = new Manager;
            $fractal->setSerializer(new ArraySerializer);
            $resource = new Collection($data, $transformer);
            $data = $fractal->createData($resource)->toArray();
        }

        return new JsonResponse($data, 200);
    }
}
