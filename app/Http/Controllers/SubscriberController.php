<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\document;
use App\Subscriptions\SubscriptionManager;
use App\Subscriptions\SubscriptionException;

class SubscriberController extends Controller{

    public function __construct()
    {
        $this->subscriptionManager = new SubscriptionManager;
    }

    public function showDashboard(Request $request)
    {
        $subscriptions = $this->subscriptionManager->getSubscriptions($request->user());
        $documents = $this->subscriptionManager->getSubscriptionOptions(
            $request->user()->id
        )->get();

        return view('subscriber/dashboard', [
            'subscriptions' => $subscriptions,
            'documents' => $documents
        ]);
    }

    public function searchDocument(Request $request)
    {
        $documents = $this->subscriptionManager->getSubscriptionOptions(
            $request->user()->id,
            $request->input('q')
        )->get();

        return view('subscriber/search', [
            'documents' => $documents
        ]);
    }

    public function subscribeToDocument(Request $request, $id)
    {
        try{
            $document = $this->subscriptionManager->subscribeToDocument($request->user(), $id);

            return redirect()->action('SubscriberController@showDashboard')
                ->with('status', 'Subscribed to '.$document->title);
        }
        catch(SubscriptionException $ex){
            return back()->with('error', $ex->getMessage());
        }

    }

    public function unsubscribeToDocument(Request $request, $id)
    {
        try{
            $document = $this->subscriptionManager->unsubscribeToDocument($request->user(), $id);

            return redirect()->action('SubscriberController@showDashboard')
                ->with('status', 'Unsubscribed from '.$document->title);
        }
        catch(SubscriptionException $ex){
            return back()->with('error', $ex->getMessage());
        }

    }
}
