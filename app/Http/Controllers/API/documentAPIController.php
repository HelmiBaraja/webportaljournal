<?php

namespace App\Http\Controllers\API;

use App\Http\Requests\API\CreatedocumentAPIRequest;
use App\Http\Requests\API\UpdatedocumentAPIRequest;
use App\Models\document;
use App\Repositories\documentRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use InfyOm\Generator\Criteria\LimitOffsetCriteria;
use InfyOm\Generator\Utils\ResponseUtil;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

/**
 * Class documentController
 * @package App\Http\Controllers\API
 */

class documentAPIController extends InfyOmBaseController
{
    /** @var  documentRepository */
    private $documentRepository;

    public function __construct(documentRepository $documentRepo)
    {
        $this->documentRepository = $documentRepo;
    }

    /**
     * @param Request $request
     * @return Response
     *
     * @SWG\Get(
     *      path="/documents",
     *      summary="Get a listing of the documents.",
     *      tags={"document"},
     *      description="Get all documents",
     *      produces={"application/json"},
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/document")
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function index(Request $request)
    {
        $this->documentRepository->pushCriteria(new RequestCriteria($request));
        $this->documentRepository->pushCriteria(new LimitOffsetCriteria($request));
        $documents = $this->documentRepository->all();

        return $this->sendResponse($documents->toArray(), 'documents retrieved successfully');
    }

    /**
     * @param CreatedocumentAPIRequest $request
     * @return Response
     *
     * @SWG\Post(
     *      path="/documents",
     *      summary="Store a newly created document in storage",
     *      tags={"document"},
     *      description="Store document",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="document that should be stored",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/document")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/document"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function store(CreatedocumentAPIRequest $request)
    {
        $input = $request->all();

        $documents = $this->documentRepository->create($input);

        return $this->sendResponse($documents->toArray(), 'document saved successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Get(
     *      path="/documents/{id}",
     *      summary="Display the specified document",
     *      tags={"document"},
     *      description="Get document",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of document",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/document"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function show($id)
    {
        /** @var document $document */
        $document = $this->documentRepository->find($id);

        if (empty($document)) {
            return Response::json(ResponseUtil::makeError('document not found'), 404);
        }

        return $this->sendResponse($document->toArray(), 'document retrieved successfully');
    }

    /**
     * @param int $id
     * @param UpdatedocumentAPIRequest $request
     * @return Response
     *
     * @SWG\Put(
     *      path="/documents/{id}",
     *      summary="Update the specified document in storage",
     *      tags={"document"},
     *      description="Update document",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of document",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="document that should be updated",
     *          required=false,
     *          @SWG\Schema(ref="#/definitions/document")
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  ref="#/definitions/document"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function update($id, UpdatedocumentAPIRequest $request)
    {
        $input = $request->all();

        /** @var document $document */
        $document = $this->documentRepository->find($id);

        if (empty($document)) {
            return Response::json(ResponseUtil::makeError('document not found'), 404);
        }

        $document = $this->documentRepository->update($input, $id);

        return $this->sendResponse($document->toArray(), 'document updated successfully');
    }

    /**
     * @param int $id
     * @return Response
     *
     * @SWG\Delete(
     *      path="/documents/{id}",
     *      summary="Remove the specified document from storage",
     *      tags={"document"},
     *      description="Delete document",
     *      produces={"application/json"},
     *      @SWG\Parameter(
     *          name="id",
     *          description="id of document",
     *          type="integer",
     *          required=true,
     *          in="path"
     *      ),
     *      @SWG\Response(
     *          response=200,
     *          description="successful operation",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="success",
     *                  type="boolean"
     *              ),
     *              @SWG\Property(
     *                  property="data",
     *                  type="string"
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string"
     *              )
     *          )
     *      )
     * )
     */
    public function destroy($id)
    {
        /** @var document $document */
        $document = $this->documentRepository->find($id);

        if (empty($document)) {
            return Response::json(ResponseUtil::makeError('document not found'), 404);
        }

        $document->delete();

        return $this->sendResponse($id, 'document deleted successfully');
    }
}
