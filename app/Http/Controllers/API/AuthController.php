<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subscriptions\SubscriptionManager;
use App\Subscriptions\SubscriptionException;
use App\Subscriptions\SubscriptionTransformer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\JsonResponse;

class AuthController extends Controller{

    public function login(Request $request)
    {
        $this->validate($request, [
            'userName' => 'required',
            'password' => 'required'
        ]);

        if( Auth::attempt([
            'email' => $request->input('userName'),
            'password' => $request->input('password')]) ){

            $user = Auth::user();
            $token = \App::make('App\TokenManager')->generateToken($user->id);
            return [
                'accessToken' => $token,
                'userId' => $user->id,
                'username' => $user->email,
                'userType' => $user->role
            ];
        }

        return new JsonResponse(['error' => 'Auth failed'], 401);
    }

}
