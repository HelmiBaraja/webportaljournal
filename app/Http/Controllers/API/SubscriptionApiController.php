<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subscriptions\SubscriptionManager;
use App\Subscriptions\SubscriptionException;
use App\Subscriptions\SubscriptionTransformer;

class SubscriptionAPIController extends Controller{

    public function __construct()
    {
        $this->subscriptionManager = new SubscriptionManager;
    }

    public function getSubscription(Request $request)
    {
        $subscriptions = $this->subscriptionManager->getSubscriptions($request->user());
        $transformer = new SubscriptionTransformer;
        return $this->renderJson($subscriptions, $transformer);
    }

    public function viewFile(Request $request, $id)
    {
        $document = \App\Models\document::findOrFail($id);
        $response = response()->download(storage_path('pdfLibrary/' . $document->filename));
        return $response;
    }

}
