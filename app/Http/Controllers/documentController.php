<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreatedocumentRequest;
use App\Http\Requests\UpdatedocumentRequest;
use App\Repositories\documentRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use App\Criteria\PublisherCriteria;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

use Auth;
//MODEL
use App\Models\document;

class documentController extends InfyOmBaseController
{
    /** @var  documentRepository */
    private $documentRepository;

    public function __construct(documentRepository $documentRepo)
    {
        $this->documentRepository = $documentRepo;
        $this->documentRepository->pushCriteria(new PublisherCriteria);
    }

    /**
     * Display a listing of the document.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->documentRepository->pushCriteria(new RequestCriteria($request));
        $documents = $this->documentRepository->all();

        return view('documents.index')
            ->with('documents', $documents);
    }

    /**
     * Show the form for creating a new document.
     *
     * @return Response
     */
    public function create()
    {
        return view('documents.create');
    }

    /**
     * Store a newly created document in storage.
     *
     * @param CreatedocumentRequest $request
     *
     * @return Response
     */
    public function store(CreatedocumentRequest $request)
    {
        // $input = $request->all();
        // dd($request->all());
        //
        // $document = $this->documentRepository->create($input);


        //uploading file pdf
        $fileName = $request->file('document')->getFileName();;
        $fileExtension = $request->file('document')->getClientOriginalExtension();
        $request->file('document')->move(storage_path('pdfLibrary'), $fileName.'.'.$fileExtension);

        $file = $fileName.'.'.$fileExtension;

        $document = new document();
        $document->title = $request->title;
        $document->status = $request->status;
        $document->filename = $file;
        $document->publisher_id = Auth::user()->id;
        $document->save();


        Flash::success('document saved successfully.');

        return redirect(route('documents.index'));
    }

    /**
     * Display the specified document.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $document = $this->documentRepository->findWithoutFail($id);

        if (empty($document)) {
            Flash::error('document not found');

            return redirect(route('documents.index'));
        }

        return view('documents.show')->with('document', $document);
    }

    /**
     * Show the form for editing the specified document.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $document = $this->documentRepository->findWithoutFail($id);

        if (empty($document)) {
            Flash::error('document not found');

            return redirect(route('documents.index'));
        }

        return view('documents.edit')->with('document', $document);
    }

    /**
     * Update the specified document in storage.
     *
     * @param  int              $id
     * @param UpdatedocumentRequest $request
     *
     * @return Response
     */
    public function update($id, UpdatedocumentRequest $request)
    {
        $document = $this->documentRepository->findWithoutFail($id);

        if (empty($document)) {
            Flash::error('document not found');

            return redirect(route('documents.index'));
        }

        $document = $this->documentRepository->update($request->all(), $id);

        Flash::success('document updated successfully.');

        return redirect(route('documents.index'));
    }

    /**
     * Remove the specified document from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $document = $this->documentRepository->findWithoutFail($id);

        if (empty($document)) {
            Flash::error('document not found');

            return redirect(route('documents.index'));
        }

        $this->documentRepository->delete($id);

        Flash::success('document deleted successfully.');

        return redirect(route('documents.index'));
    }
}
