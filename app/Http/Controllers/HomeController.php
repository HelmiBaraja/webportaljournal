<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    /**
     * Show the application index page.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if( Auth::guest() ){
            return redirect()->guest('login');
        }

        if( $request->user()->is_publisher ){
            return redirect('/documents');
        }
        return redirect('/dashboard');
    }
}
