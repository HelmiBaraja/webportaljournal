<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\TokenManager;

class AuthenticateSubscriber
{

    public function __construct(TokenManager $tokenManager)
    {
        $this->tokenManager = $tokenManager;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        $token = $request->header('accessToken');
        if( !is_null($token) ){
            $userId = $this->tokenManager->verifyToken($token);
            if( $userId ){
                Auth::loginUsingId($userId);
            }
        }


        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login');
            }
        }

        $user = Auth::guard($guard)->user();
        if($user->is_publisher)
        {
            Auth::guard($guard)->logout();
            return redirect('login');
        }

        return $next($request);
    }
}
